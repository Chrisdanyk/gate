/**
 * View Models used by Spring MVC REST controllers.
 */
package cd.home.web.rest.vm;
